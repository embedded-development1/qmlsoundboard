import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtMultimedia

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("SoundBoard")
    ColumnLayout{
        anchors.fill: parent
        anchors.margins: 20
        GridLayout{
            id: grid
            Layout.fillWidth: true
            Layout.fillHeight: true
            anchors.margins: 10
            columns: 4
            rows: 4
            SoundButton{
                buttonColor: "blue"
                buttonText: "Arcade"
                fileSource: "qrc:/SoundEffects/arcade.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "red"
                buttonText: "Bird1Chirp"
                fileSource: "qrc:/SoundEffects/birdChirp.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "green"
                buttonText: "Bird2Chirp"
                fileSource: "qrc:/SoundEffects/birdsChirp.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "yellow"
                buttonText: "Chord"
                fileSource: "qrc:/SoundEffects/chordSwell.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "orange"
                buttonText: "Flute1"
                fileSource: "qrc:/SoundEffects/flute.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "purple"
                buttonText: "Flute2"
                fileSource: "qrc:/SoundEffects/flutes.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "cyan"
                buttonText: "Guitar"
                fileSource: "qrc:/SoundEffects/guitar.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "violet"
                buttonText: "Hawk"
                fileSource: "qrc:/SoundEffects/hawk.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "skyblue"
                buttonText: "Heaven"
                fileSource: "qrc:/SoundEffects/heaven.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "teal"
                buttonText: "Orchestra1"
                fileSource: "qrc:/SoundEffects/orchestra1.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "lightcoral"
                buttonText: "Orchestra2"
                fileSource: "qrc:/SoundEffects/orchestra2.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "steelblue"
                buttonText: "Sea bird"
                fileSource: "qrc:/SoundEffects/seabird.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "forestgreen"
                buttonText: "Tropical bird"
                fileSource: "qrc:/SoundEffects/tropicalbird.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "gold"
                buttonText: "Trumpets"
                fileSource: "qrc:/SoundEffects/trumpets.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "sienna"
                buttonText: "Violin"
                fileSource: "qrc:/SoundEffects/violin.wav"
                volume: volumeSlider
            }
            SoundButton{
                buttonColor: "slategray"
                buttonText: "Wind Metal"
                fileSource: "qrc:/SoundEffects/windmetal.wav"
                volume: volumeSlider
            }
        }
        ColumnLayout{
            Layout.fillWidth: true
            Label{
                text: "Volume: "
                font.pixelSize: parent.height / 2.5
            }

            Slider{
                id:volumeSlider
                value: 1
                Layout.fillWidth: true
            }
        }
    }
}
