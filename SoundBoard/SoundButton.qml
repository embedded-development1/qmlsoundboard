import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtMultimedia

Rectangle{
    id:rootbutton
    property color buttonColor: "red"
    property string buttonText: "arcade"
    property url fileSource: ""
    required property Slider volume
    property real volumevalue: volume.value

    width: grid.width
    height: grid.height
    Layout.fillWidth: true
    Layout.fillHeight: true
    color: buttonColor
    radius: 20
    Text{
        id:buttontext
        text: buttonText
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: rootbutton.width / 8
    }

    MediaPlayer{
        id:buttonsound
        source: fileSource
        audioOutput: AudioOutput{
            volume: volumevalue
        }
    }
    MouseArea{
        anchors.fill: parent
        onClicked: {
            buttonsound.play()
            buttonanim.start()
        }
    }
    SequentialAnimation{
        id:buttonanim
        PropertyAnimation{
            target: rootbutton
            property: "color"
            to: Qt.lighter(rootbutton.color, 1.5)
            duration: 100
        }
        PauseAnimation {
            duration: buttonsound.duration
        }
        PropertyAnimation{
            target: rootbutton
            property: "color"
            to: buttonColor
            duration: 100
        }
    }
}
